/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@uptime.link/detector',
  version: '2.0.1',
  description: 'a detector for answering network questions locally. It does not rely on any online services.'
}
